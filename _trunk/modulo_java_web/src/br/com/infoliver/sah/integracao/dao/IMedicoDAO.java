package br.com.infoliver.sah.integracao.dao;

import java.util.List;

import br.com.infoliver.sah.negocio.entity.Medico;

public interface IMedicoDAO {
	List<Medico> listar();
	
	Medico consultar(Integer sequencial);
}