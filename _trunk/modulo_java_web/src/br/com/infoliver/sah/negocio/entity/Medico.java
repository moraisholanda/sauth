package br.com.infoliver.sah.negocio.entity;

import java.io.Serializable;

import br.com.infoliver.sah.configuracao.validacao.EntityValidador;
import br.com.infoliver.sah.configuracao.validacao.GeneroEntity;

@EntityValidador(entidade = "Médico", genero = GeneroEntity.Masculino)
public class Medico implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer sequencial;
	private String nome;
	private String cns;
	private Integer numeroAtendimentoMes;
	private String indicadorAtivo;
	private Ocupacao ocupacao;
	
	public Medico() {
	}

	public Integer getSequencial() {
		return sequencial;
	}

	public void setSequencial(Integer sequencial) {
		this.sequencial = sequencial;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCns() {
		return cns;
	}

	public void setCns(String cns) {
		this.cns = cns;
	}

	public Integer getNumeroAtendimentoMes() {
		return numeroAtendimentoMes;
	}

	public void setNumeroAtendimentoMes(Integer numeroAtendimentoMes) {
		this.numeroAtendimentoMes = numeroAtendimentoMes;
	}

	public String getIndicadorAtivo() {
		return indicadorAtivo;
	}

	public void setIndicadorAtivo(String indicadorAtivo) {
		this.indicadorAtivo = indicadorAtivo;
	}

	public Ocupacao getOcupacao() {
		return ocupacao;
	}

	public void setOcupacao(Ocupacao ocupacao) {
		this.ocupacao = ocupacao;
	}
}