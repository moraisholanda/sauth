package infoliver.control{
	import leonelcasado.com.adobe.cairngorm.control.FrontController;
	import infoliver.model.events.*;
	
	public class FController extends FrontController{
		
		public function FController(){
			registerCommadClass(ArquivoEvent);
			registerCommadClass(AutenticacaoEvent);
			registerCommadClass(EmpresaEvent);
			registerCommadClass(EncaminhadorEvent);
			registerCommadClass(EscolaridadeEvent);
			registerCommadClass(GrupoEvent);
			registerCommadClass(GrupoLaudoEvent);
			registerCommadClass(GrupoUsuarioEvent);
			registerCommadClass(GrupoPermissaoEvent);
			registerCommadClass(LogAcessoEvent);
			registerCommadClass(MedicoEvent);
			registerCommadClass(OcupacaoEvent);
			registerCommadClass(PacienteEvent);
			registerCommadClass(PermissaoEvent);
			registerCommadClass(ProcedimentoMedicoEvent);
			registerCommadClass(RacaEvent);
			registerCommadClass(RelatorioEvent);
			registerCommadClass(TipoResponsavelEvent);
			registerCommadClass(UsuarioEvent);
		}		
	}
}